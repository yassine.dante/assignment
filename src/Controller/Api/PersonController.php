<?php

namespace App\Controller\Api;

use App\Entity\Person;
use App\Exception\ApiProblem;
use App\Exception\ApiProblemException;
use App\Service\PersonService;
use App\Traits\PaginationInfoTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PersonController extends AbstractController
{
    use PaginationInfoTrait;

    private EntityManagerInterface $em;

    private SerializerInterface $serializer;

    private PersonService $personService;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer, PersonService $personService)
    {
        $this->em = $entityManager;
        $this->serializer = $serializer;
        $this->personService = $personService;
    }

    #[Route(path: '/api/person', name: 'person_list_api', methods: ['GET'])]
    public function personsList(Request $request): JsonResponse
    {
        $paginationInfo = $this->getPaginationInfo($request);
        $page = $paginationInfo['page'];
        $limit = $paginationInfo['limit'];

        $data = $this->personService->getPersons($page, $limit);

        $data = $this->serializer->serialize($data, 'json', ['groups' => 'person_collection']);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    #[Route(path: '/api/person/{id}', methods: ['GET'])]
    public function getPerson($id): JsonResponse
    {
        $person = $this->em->getRepository(Person::class)->find((int) $id);
        if (!$person) {
            $apiProblem = new ApiProblem(Response::HTTP_NOT_FOUND, ApiProblem::RESOURCE_NOT_FOUND);
            throw new ApiProblemException($apiProblem);
        }

        $data = $this->serializer->serialize($person, 'json', ['groups' => 'person_item']);

        return new JsonResponse($data, Response::HTTP_OK, [], true);

    }
}