<?php

namespace App\Traits;

use App\Constants\Pagination;
use Symfony\Component\HttpFoundation\Request;

trait PaginationInfoTrait
{


    public function getPaginationInfo(Request $request): array
    {
        $page = $request->get('page', Pagination::DEFAULT_PAGE);
        $limit = $request->get('limit', Pagination::DEFAULT_LIMIT);
        $page = (is_numeric($page) && $page > 0) ? $page : Pagination::DEFAULT_PAGE;
        $limit = (is_numeric($limit) && $limit > 0) ? $limit : Pagination::DEFAULT_LIMIT;

        return [
            'page' => $page,
            'limit' => $limit,
        ];
    }

}