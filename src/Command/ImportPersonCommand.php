<?php

namespace App\Command;

use App\Message\ImportPersons;
use App\Service\PersonImporterInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraints\DateTime;

#[AsCommand(
    name: 'app:import-person',
    description: 'Import members of the european parliament',
)]
class ImportPersonCommand extends Command
{
    private PersonImporterInterface $personImporter;

    private MessageBusInterface $messageBus;

    public function __construct(PersonImporterInterface $personImporter, MessageBusInterface $messageBus)
    {
        $this->personImporter = $personImporter;
        $this->messageBus = $messageBus;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $now = new \DateTime();
        $output->writeln([
            'Command has started at ' . $now->format('Y-m-d H:i:s')
        ]);
        $message = new ImportPersons();
        $this->messageBus->dispatch($message);
        $now = new \DateTime();
        $output->writeln([
            'Command terminated at ' . $now->format('Y-m-d H:i:s')
        ]);

        return Command::SUCCESS;
    }
}
