<?php

namespace App\Factory;

use App\Entity\Person;

class PersonFactory
{
    private ContactFactory $contactFactory;

    public function __construct(ContactFactory $contactFactory)
    {
        $this->contactFactory = $contactFactory;
    }

    public function build(
        ?int $externalId,
        ?string $fullName,
        ?string $country,
        ?string $politicalGroup,
        ?string $nationalPoliticalGroup,
        array $contacts

    ): Person {
        $person = new Person();
        $fullName = explode(' ', $fullName);
        $firstName = $fullName[0] ?? null;
        $lastName = $fullName[1] ?? null;

        $person->setExternalId($externalId)
               ->setFirstName($firstName)
               ->setLastName($lastName)
               ->setCountry($country)
               ->setPoliticalGroup($politicalGroup)
               ->setNationalPoliticalGroup($nationalPoliticalGroup);

        if (!empty($contacts)) {
            $this->AddContacts($contacts, $person);
        }

        return $person;
    }

    private function AddContacts(array $contacts, Person $person)
    {
           foreach ($contacts as $contactItem) {
               $contact = $this->contactFactory->build($contactItem['title'], $contactItem['value']);
               $person->addContact($contact);
           }

    }
}