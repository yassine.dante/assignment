<?php

namespace App\Factory;

use App\Entity\Contact;

class ContactFactory
{

    public function build(string $type, string $value): Contact
    {
        $contact = new Contact();

        $contactType = match($type) {
            'LinkedIn', 'Facebook', 'Twitter', 'Website', 'Instagram', 'Youtube' => Contact::SOCIAL,
            'E-mail' => Contact::EMAIL,
            'address' => Contact::ADDRESS,
            default => 'Other'
        };

        $contact->setType($contactType)
            ->setValue($value);

        return $contact;

    }
}