<?php

namespace App\Exception;

use http\Exception\InvalidArgumentException;

class ApiProblem
{
    public const RESOURCE_NOT_FOUND = 3000;

    public static array $titles = [
        self::RESOURCE_NOT_FOUND => 'Resource Not found',
    ];

    private array $extraData = [];

    private $statusCode;

    private $type;

    private $title;

    public function __construct($statusCode, $type)
    {
        $this->statusCode = $statusCode;
        $this->type = $type;

        if (!isset(static::$titles[$type])) {
            throw new InvalidArgumentException('No title for type' . $type);
        }

        $this->title = static::$titles[$type];
    }

    public function toArray()
    {
        return array_merge(
            $this->extraData,
            [
                'errors' => [
                    'status' => $this->statusCode,
                    'type' => $this->type,
                    'title' => $this->title
                ]
            ]
        );
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }


    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): ApiProblem
    {
        $this->type = $type;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): ApiProblem
    {
        $this->title = $title;
        return $this;
    }
}
