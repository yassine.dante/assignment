<?php

namespace App\Service;

interface PersonImporterInterface
{
    public function import() : int;

    public function getContent() : array;
}