<?php

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PersonService
{
    private EntityManagerInterface $em;

    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->em = $entityManager;
        $this->serializer = $serializer;
    }

    public function getPersons(int $page, int $limit)
    {

        $persons = $this->em->getRepository(Person::class)->findPersons($page, $limit);
        $countPersons = $this->em->getRepository(Person::class)->countPersons();

        return [
            'persons' => $persons,
            'page' => $page,
            'totalPages' => ($countPersons % $limit === 0 ) ? ($countPersons / $limit) : (int)($countPersons / $limit) + 1
        ];
    }
}