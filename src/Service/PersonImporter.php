<?php

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use App\Factory\PersonFactory;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PersonImporter implements PersonImporterInterface
{
    public const BASE_URL = 'https://www.europarl.europa.eu/meps/en';
    public const PERSONS_LIST_URI = '/full-list/xml';

    private HttpClientInterface $httpClient;

    private EntityManagerInterface $em;

    private PersonFactory $personFactory;


    public function __construct
    (
        HttpClientInterface $httpClient,
        EntityManagerInterface $entityManager,
        PersonFactory $personFactory,
    ) {
        $this->httpClient = $httpClient;
        $this->em = $entityManager;
        $this->personFactory = $personFactory;
    }

    public function import(): int
    {
        $data = $this->getContent();
        $counter = 0;
        foreach ($data as $item) {
            $person = $this->em->getRepository(Person::class)->findPersonByExternalId($item['externalId']);
            if (!$person) {
                $person = $this->personFactory->build(
                    $item['externalId'],
                    $item['fullName'],
                    $item['country'],
                    $item['politicalGroup'],
                    $item['nationalPoliticalGroup'],
                    $item['contacts']
                );
                $counter++;
                $this->em->persist($person);
            }
        }

        $this->em->flush();

        return $counter;
    }

    public function getContent(): array
    {
        $response = $this->httpClient->request('GET', self::BASE_URL . self::PERSONS_LIST_URI);

        $content = $response->getContent();
        $crawler = new Crawler($content);

        return $crawler->filter('mep')->each(
            function (Crawler $node) {
                $fullName = $node->filter('fullName')->text();
                $country = $node->filter('country')->text();
                $politicalGroup = $node->filter('politicalGroup')->text();
                $externalId = $node->filter('id')->text();
                $nationalPoliticalGroup = $node->filter('nationalPoliticalGroup')->text();
                $contacts = $this->getContactLinks($externalId, $fullName);

                return [
                    'externalId' => $externalId,
                    'fullName'=> $fullName,
                    'country' => $country,
                    'politicalGroup' => $politicalGroup,
                    'nationalPoliticalGroup' => $nationalPoliticalGroup,
                    'contacts' => $contacts
                ];
            }
        );
    }

    public function getContactLinks(int $externalId, string $fullName): array
    {
        $fullName = str_replace(' ', '_', $fullName);
        $url = self::BASE_URL . '/' . $externalId . '/' . $fullName . '/home';
        $response = $this->httpClient->request('GET', $url);
        $content = $response->getContent();
        $crawler = new Crawler($content);

        $socialLinksContainer = $crawler->filter('.erpl_social-share-horizontal');
        $contactLinks = [];
        $socialLinksContainer->filter('a')->each(function (Crawler $link) use (&$contactLinks) {
            $value = $link->attr('href');
            $title = $link->attr('data-original-title');
            $contactLinks[] = [
                'title' => $title,
                'value' => $value
            ];
        });

        $address = $crawler->filter('div.erpl_title-h3.mt-1.mb-1')->text();
        if (!$address) {
            $contactLinks[] = [
                'title' => 'address',
                'value' => $address
            ];
        }

        return $contactLinks;
    }

}