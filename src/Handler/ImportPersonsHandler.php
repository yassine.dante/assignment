<?php

namespace App\Handler;

use App\Message\ImportPersons;
use App\Service\PersonImporter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ImportPersonsHandler implements MessageHandlerInterface
{
    private PersonImporter $personImporter;

    private LoggerInterface $logger;

    public function __construct(PersonImporter $personImporter, LoggerInterface $logger)
    {
        $this->personImporter = $personImporter;
        $this->logger = $logger;
    }

    public function __invoke(ImportPersons $importPersons)
    {
        $personNb = $this->personImporter->import();
        $this->logger->info($personNb . ' new person(s) imported successfully');
    }
}