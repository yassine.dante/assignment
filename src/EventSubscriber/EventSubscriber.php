<?php

namespace App\EventSubscriber;

use App\Exception\ApiProblemException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class EventSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'apiProblemException',
        ];
    }

    public function apiProblemException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if ($exception instanceof ApiProblemException) {
            $apiProblem = $exception->getApiProblem();

            $response = new JsonResponse(
                $apiProblem->toArray(),
                $apiProblem->getStatusCode()
            );

            $response->headers->set('Content-type', 'application/problem+json');
            $event->setResponse($response);
        }
    }
}