<?php

namespace App\Repository;

use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Person>
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    public function findPersonByExternalId(?int $externalId)
    {
        return $this->createQueryBuilder('person')
                    ->where('person.externalId = :id')
                    ->setParameter('id', $externalId)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    public function findPersons(int $page, int $limit)
    {
        return $this->findPersonsQueryBuilder()
                    ->setFirstResult(($page - 1) * $limit)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult();

    }

    public function findPersonsQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('person');
    }

    public function countPersons()
    {
        return $this->findPersonsQueryBuilder()
                    ->select('COUNT(person.id) as count')
                    ->getQuery()
                    ->getSingleResult()['count'];
    }
}
