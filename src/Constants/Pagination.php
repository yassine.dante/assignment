<?php

namespace App\Constants;

final class Pagination
{
    public const DEFAULT_PAGE = 1;

    public const DEFAULT_LIMIT = 5;
}