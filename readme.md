<h1>Steps to run the project</h1>
<ul>
<li>composer install</li>
<li>php bin/console doctrine:database:create</li>
<li>php bin/console make:migration </li>
<li>php bin/console doctrine:migrations:migrate</li>
<li>docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3.9-management
</li>
<li>php bin/console messenger:consume async -vv</li>

</ul>

<h1>Run the Command to import persons </h1>

php bin/console app:import-person


<h1>Api's</h1>

<ul>
<li> /api/person</li>
<li>/api/person/{id}</li>
</ul>